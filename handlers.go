package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

// SemVerHandler handles requests made to the API
func SemVerHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "POST":
		d := json.NewDecoder(io.LimitReader(r.Body, 65536))

		var svr SemVerRequest

		err := d.Decode(&svr)
		if err != nil || err == io.EOF {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		if svr.Version == "" {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("Required 'version' field not set"))
			return
		}

		sv, err := MatchVersion(svr.Version)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("Version improperly formatted"))
			return
		}
		BumpVersion(svr.Message, &sv)
		w.Write([]byte(fmt.Sprintf("%d.%d.%d", sv.Major, sv.Minor, sv.Patch)))

	default:
		w.WriteHeader(http.StatusNotImplemented)
		w.Write([]byte("Method not supported, only POST!"))
	}
}
