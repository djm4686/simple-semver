# Simple Semantic Versioning
`simple-semver` is a simple semantic versioning API. Requests are sent to
`/semver` via JSON with an optional message, and version (tag). The API returns
a bumped version string of the sent request. The aim of this was to have a
simple API that can be publicly accessed to run requests against as part of CI
pipelines.

## Example `POST`
```bash
$ curl -H "Content-Type: application/json" -d '{"version": "0.11.2", "message": "#major"}' -X POST localhost:8080/semver

# Response
1.0.0
```

# Issues
See the [Issues](https://gitlab.com/bdebyl/simple-semver/issues) to track the
features to be implemented along with requesting others.
