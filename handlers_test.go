package main

import (
	"bytes"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

type goodJSONTest struct {
	json []byte
	out  string
}

var methodTests = []string {
	"CONNECT",
	"DELETE",
	"GET",
	"HEAD",
	"OPTIONS",
	"PATCH",
	"PUT",
	"TRACE",
}

var badJSONTests = [][]byte {
	[]byte(`{}`),
	[]byte(`{"hello": "world"}`),
	[]byte(`{"message": "#patch"}`),
}

var versionTests = [][]byte {
	[]byte(`{"version": "a.b.c"}`),
	[]byte(`{"version": "1.2c"}`),
	[]byte(`{"version": "v12"}`),
}

var goodJSONTests = []goodJSONTest {
	{[]byte(`{"version": "1.0.2"}`), "1.0.3"},
	{[]byte(`{"message": "testing", "version": "0.1.2"}`), "0.1.3"},
	{[]byte(`{"message": "#patch", "version": "4.0.6"}`), "4.0.7"},
	{[]byte(`{"message": "#minor", "version": "1.2.3"}`), "1.3.0"},
	{[]byte(`{"message": "#major", "version": "3.2.1"}`), "4.0.0"},
	{[]byte(`{"message": "#patch #minor #major", "version": "1.1.1"}`), "2.0.0"},
	{[]byte(`{"message": "#patch #minor", "version": "2.2.2"}`), "2.3.0"},
}

func createRequest(method string, buf []byte) (*http.Request, error) {
	return http.NewRequest(method, "/semver", bytes.NewBuffer(buf))
}

func semVerResponse(req *http.Request) *http.Response {
	rec := httptest.NewRecorder()
	SemVerHandler(rec, req)

	return rec.Result()
}

func bodyContains(body io.Reader, s string) bool {
	buf := new(bytes.Buffer)
	buf.ReadFrom(body)
	return strings.Contains(buf.String(), s)
}

func TestMethodNotSupported(t *testing.T) {
	for _, method := range methodTests {
		req, err := createRequest(method, nil)
		if err != nil {
			t.Errorf("Error creating req: %v", req)
		}

		res := semVerResponse(req)
		if res.StatusCode != http.StatusNotImplemented {
			t.Errorf("Expected NotImplemted for request: %v, got: %v", req, res)
		}

		if !bodyContains(res.Body, "not supported") {
			t.Errorf("Could not find 'not supported' in '%s'", res.Body)
		}
	}
}

func TestEmpty(t *testing.T) {
	req, err := createRequest("POST", []byte(``))
	if err != nil {
		t.Error("Failed to create POST request!")
	}

	res := semVerResponse(req)
	if res.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected BadRequest for request: %v, got %v", req, res)
	}
}

func TestBadJSON(t *testing.T) {
	for _, test := range badJSONTests {
		req, err := createRequest("POST", test)
		if err != nil {
			t.Error("Failed to create POST request!")
		}

		res := semVerResponse(req)
		if res.StatusCode != http.StatusBadRequest {
			t.Errorf("Expected BadRequest for request: %v, got %v", req, res)
		}
	}
}

func TestBadVersion(t *testing.T) {
	for _, test := range versionTests {
		req, err := createRequest("POST", test)
		if err != nil {
			t.Error("Failed to create POST request!")
		}

		res := semVerResponse(req)
		if  res.StatusCode != http.StatusBadRequest {
			t.Errorf("Expected BadRequest for request: %v, got %v", req, res)
		}

		if !bodyContains(res.Body, "improperly formatted") {
			t.Errorf("Could not find 'improperly formatted' in '%s'", res.Body)
		}
	}
}

func TestSendJSON(t *testing.T) {

	for _, test := range goodJSONTests {
		req, err := createRequest("POST", test.json)
		if err != nil {
			t.Error("Failed to create Post request!")
		}
		req.Header.Set("Content-Type", "application/json")

		res := semVerResponse(req)
		if res.StatusCode != http.StatusOK {
			t.Errorf("Expected OK for request: %v, got %v", req, res)
		}

		if !bodyContains(res.Body,  test.out) {
			t.Errorf("Could not find 'improperly formatted' in '%s'", res.Body)
		}
	}
}
