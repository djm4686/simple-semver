package main

import "testing"

var errTmpl = "%s version failed to bump! given: %+v, expected: %+v"
var errMsgTmpl = "'%s' failed! given: %+v, expected %+v"

type SemVerTest struct {
	in, out SemVer
}

type versionBumpTest struct {
	msg string
	svt SemVerTest
}

var bumpMajorTests = []SemVerTest {
	{ SemVer{ 0, 0, 0 }, SemVer{ 1, 0, 0 } },
	{ SemVer{ 2, 50, 1 }, SemVer{ 3, 0, 0 } },
	{ SemVer{ 999, 124, 0 }, SemVer{ 1000, 0, 0 } },
}

var bumpMinorTests = []SemVerTest {
	{ SemVer{ 0, 0, 0 }, SemVer{ 0, 1, 0 } },
	{ SemVer{ 3, 50, 1 }, SemVer{ 3, 51, 0 } },
	{ SemVer{ 1010, 124, 55 }, SemVer{ 1010, 125, 0 } },
}

var bumpPatchTests = []SemVerTest {
	{ SemVer{ 0, 0, 0 }, SemVer{ 0, 0, 1 } },
	{ SemVer{ 3, 50, 1 }, SemVer{ 3, 50, 2 } },
	{ SemVer{ 444, 0, 505 }, SemVer{ 444, 0, 506 } },
}

var bumpVersionTests = []versionBumpTest {
	{
		"some major change #major",
		SemVerTest{SemVer{0,0,0}, SemVer{1,0,0}},
	}, {
		"some #minor change",
		SemVerTest{SemVer{0,0,0}, SemVer{0,1,0}},
	}, {
		"bug #patch fix",
		SemVerTest{SemVer{0,0,0}, SemVer{0,0,1}},
	}, {
		"#minor change but bump #major",
		SemVerTest{SemVer{1,0,0}, SemVer{2,0,0}},
	}, {
		"#patch #major (first) #minor",
		SemVerTest{SemVer{4,2,3}, SemVer{5,0,0}},
	}, {
		"#patch major major# #minor (first)",
		SemVerTest{SemVer{0,3,2}, SemVer{0,4,0}},
	}, {
		"#patch (only) MINOR major# # minor",
		SemVerTest{SemVer{0,3,2}, SemVer{0,3,3}},
	}, {
		"(#minor) message oops #patch",
		SemVerTest{SemVer{1,1,1}, SemVer{1,2,0}},
	}, {
		"(major) message oops no # (should patch; default)",
		SemVerTest{SemVer{0,11,41}, SemVer{0,11,42}},
	},
}

func TestBumpMajor(t *testing.T) {
	for _, test := range bumpMajorTests {
		BumpMajor(&test.in)
		if test.in != test.out {
			t.Errorf(errTmpl, "Major", test.in, test.out)
		}
	}
}

func TestBumpMinor(t *testing.T) {
	for _, test := range bumpMinorTests {
		BumpMinor(&test.in)
		if test.in != test.out {
			t.Errorf(errTmpl, "Minor", test.in, test.out)
		}
	}
}

func TestBumpPatch(t *testing.T) {
	for _, test := range bumpPatchTests {
		BumpPatch(&test.in)
		if test.in != test.out {
			t.Errorf(errTmpl, "Patch", test.in, test.out)
		}
	}
}

func TestBumpVersion(t *testing.T) {
	for _, test := range bumpVersionTests {
		BumpVersion(test.msg, &test.svt.in)
		if test.svt.in != test.svt.out {
			t.Errorf(errMsgTmpl, test.msg, test.svt.in, test.svt.out)
		}
	}
}
