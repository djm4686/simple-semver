package main

import (
	"log"
	"net/http"
	"strings"
	"strconv"
	"errors"
)

// MatchVersion pulls out major, minor, patch versions for a string passed in
// of the right form. A regular expression is used to do this, meaning that
// a string of anything with 3 numerics separated by dots will be matched.
func MatchVersion(v string) (SemVer, error) {
	versionList := strings.Split(v, ".")
	err := TestVersionParsing(versionList)
	var sv SemVer
	if len(versionList) == 3 && err == nil {
		return SemVer{ParseVersionUint(versionList, 0), ParseVersionUint(versionList, 1), ParseVersionUint(versionList, 2)}, nil
	}
	return sv, errors.New("invalid version format")
}

func TestVersionParsing(versionList []string) (error) {
	for _, elem := range versionList{
		_, err := strconv.ParseUint(elem, 10, 64)
		if err != nil {
			return err
		}
	}
	return nil;
}

// Does the thing, converts a string to an int. Panics if it gets not an int.
func ParseVersionUint(versionList []string, index int) (uint64){
	val, err := strconv.ParseUint(versionList[index], 10, 64)
	if err != nil {
		panic(err)
	}
	return val;
}

// BumpMajor bumps the major version by reference of a SemVer object
func BumpMajor(sv *SemVer)  {
	sv.Major++
	sv.Minor, sv.Patch = 0, 0

	return
}

// BumpMinor bumps the minor version by reference of a SemVer object
func BumpMinor(sv *SemVer) {
	sv.Minor++
	sv.Patch = 0

	return
}

// BumpPatch bumps the patch version by reference of a SemVer object
func BumpPatch(sv *SemVer) {
	sv.Patch++

	return
}

// BumpVersion determines which portion of version to bump (default patch)
func BumpVersion(msg string, sv *SemVer) {
	switch {
	case strings.Contains(msg, "#major"):
		BumpMajor(sv)
	case strings.Contains(msg, "#minor"):
		BumpMinor(sv)
	case strings.Contains(msg, "#patch"):
		BumpPatch(sv)
	default:
		BumpPatch(sv)
	}

	return
}

func main() {
	http.HandleFunc("/semver", SemVerHandler)

	log.Fatal(http.ListenAndServe(":8080", nil))
}
